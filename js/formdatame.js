class FormDataMe extends FormData
{
	contructor()
	{
		this.methodType = 'POST';
		this.url = location.href;
		this.data = {};
		return this;
	}
	getValue(key)
    {
        return this.get(key);
    }
    getAll()
    {
        return this;
    }
    setValue(key, value)
    {
        this.append(key.toString(), value);
        return this;
    }
    merge(obj)
    {
        for(var pair of obj.entries())
        {
            this.append(pair[0], pair[1]);
        }
    }
    setUrl(url)
    {
        this.url = url;
        return this;
    }
    errorCatch(callback = null)
    {
        if(this.get('status') === 'error')
        {
        	if(typeof callback == 'function') callback();
            throw 'Ooops!';
        }
        return this;
    }
    setMethod(method = 'post')
    {
    	this.methodType = method;
    	return this;
    }
    beforeSend(callback = null)
    {
    	this.beforeSend = callback;
    	return this;
    }
    send(callback)
    {
    	if(typeof this.beforeSend == 'function') this.beforeSend();
    	$.ajax({
		    url : this.url,
		    type : this.methodType,
		    data : this,
		    processData: false,
		    contentType: false,
		    success : function(data) { if(typeof callback == 'function') callback(data); }
		});
		return this;
    }
}