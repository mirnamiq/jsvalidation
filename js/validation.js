$.prototype.validate = function ()
{
    var formDataMe = new FormDataMe(), val = '', check = true, required = false, tagName = '', name = '';
    $(this).find('select[name], input[name], textarea[name]').each(function(){
        tagName = $(this)[0].tagName.toLowerCase();
        name = $(this).attr('name');
        switch(tagName)
        {
        	case 'select': if($(this).next('span').find('span.select2-selection--single').length) val = $(this).select2Validate(); else val = $(this).selectValidate(); break;
        	case 'input': val = $(this).inputValidate(); break;
        	case 'textarea': val = $(this).textareaValidate(); break;
        	default: val = $(this).inputValidate(); break 
        }
        if(val == '') check = false;
        else formDataMe.setValue(name, val);
    });
    if(check === false)
    {
        throw 'Ooops!';
    }
    return formDataMe;
}


$.prototype.select2Validate = function($continue = true)
{
	$(this).next('span').find('span.select2-selection--single').removeClass('error-class');
    $(this).next('span').find('span.select2-selection--single').removeClass('warning-class');
    var val = $(this).select2('val'), 
    	required = $(this).is('[required]') ? true : false;
    	$check = true;
	if(typeof val == 'undefined' && val.trim() == '' && required)
	{
	    $(this).next('span').find('span.select2-selection--single').addClass('error-class');
	    $check = false;
	}
	else if(typeof val == 'undefined' && val.trim() == '')
	{
		$(this).next('span').find('span.select2-selection--single').addClass('warning-class');
	}
	if(!$continue && !$check)
	{
		throw "Ooops!";
	}
	return val;
}

$.prototype.commonValidate = function ($continue = true)
{
	$(this).removeClass('error-class');
    $(this).removeClass('warning-class');
    var val = $(this).val().trim(),
    	required = $(this).is('[required]') ? true : false;
    	$check = true;

    var maxlength = $(this).attr('maxlength');
	if(typeof maxlength != 'undefined')
	{
		if(maxlength < val.length && !$continue) $(this).validateIsImportant('Max Length!'); 
		else if(maxlength < val.length) $(this).validateIsNotImportant('Max Length!'); 
	}

	var minlength = $(this).attr('minlength');
	if(typeof minlength != 'undefined')
	{
		if(minlength < val.length && !$continue) $(this).validateIsImportant('Min Length!'); 
		else if(minlength < val.length) $(this).validateIsNotImportant('Min Length!'); 
	}


	if(val == '' && required)
	{
	    $(this).addClass('error-class');
	    $check = false;
	}
	else if(val == '')
	{
		$(this).addClass('warning-class');
	}
	if(!$continue && !$check)
	{
		throw "Ooops!";
	}	

	return val;
}

$.prototype.inputValidate = function ($continue = true) {
	switch($(this).attr('type'))
	{
		case 'text': return $(this).inputTextValidate($continue); break;
		case 'file': return $(this).inputFileValidate($continue); break;
		case 'number': return $(this).inputNumberValidate($continue); break;
		case 'password': return $(this).inputPasswordValidate($continue); break;
		case 'email': return $(this).inputEmailValidate($continue); break;
		case 'date': return $(this).inputDateValidate($continue); break;
		case 'radio': return $(this).inputRadioValidate($continue); break;
		case 'checkbox': return $(this).inputCheckboxValidate($continue); break;
		default: return $(this).inputTextValidate($continue); break;
	}
}

$.prototype.selectValidate = function ($continue = true) { return $(this).commonValidate($continue); }
$.prototype.textareaValidate = function ($continue = true) { return $(this).commonValidate($continue); }
$.prototype.textareaValidate = function ($continue = true) {
	return $(this)[0].files[0];
}
$.prototype.inputTextValidate = function ($continue = true) { return $(this).commonValidate($continue); }
$.prototype.inputPasswordValidate = function ($continue = true) { return $(this).commonValidate($continue); }
$.prototype.inputNumberValidate = function ($continue = true) {
	$(this).removeClass('.error-class');
    $(this).removeClass('.warning-class');

    required = $(this).is('[required]') ? true : false;

    var val = $(this).val();
	var check = /[^\d\.]/.test(val);
	
	if(!check && !$continue) $(this).validateIsImportant('Number!'); 
    else if(!check) $(this).validateIsNotImportant('Number!'); 

    if(isNaN(val) && !$continue) $(this).validateIsImportant('Number!'); 
    else $(this).validateIsNotImportant('Number!'); 

    if(isNaN(val)) val = 0;

    val = Number(val);

    var max = $(this).attr('max');
    if(typeof max != 'undefined')
    {
    	max = isNaN(max) ? 0 : Number(max);
    	if(max < val && $continue) $(this).validateIsImportant('Max Number!'); 
    	else if(max < val) $(this).validateIsNotImportant('Max Number!');
    }

	var min = $(this).attr('min');
    if(typeof min != 'undefined')
    {
    	min = isNaN(min) ? 0 : Number(min);
    	if(min > val && $continue) $(this).validateIsImportant('Min Number!'); 
    	else if(min > val) $(this).validateIsNotImportant('Min Number!');
    }
	return $(this).commonValidate($continue); 
}

$.prototype.inputEmailValidate = function ($continue = true)
{
	var regExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    $(this).removeClass('.error-class');
    $(this).removeClass('.warning-class');
    var check = regExp.test($(this).val());
    if(!check && !$continue)
    {
    	$(this).validateIsImportant('Email!');
    }
    else if(!check)
    {
    	$(this).validateIsNotImportant('Email!');
    }
	return $(this).commonValidate($continue); 
}
$.prototype.inputDateValidate = function ($continue = true) {
	return $(this).commonValidate($continue); 
}
$.prototype.inputCheckboxValidate = function ($continue = true) {
	return $(this).commonValidate($continue); 
}
$.prototype.inputRadioValidate = function ($continue = true) {
	return $(this).commonValidate($continue); 
}

$.prototype.validateIsImportant = function(text = '')
{
	console.log(text);
	let _class = 'warning-class';
	if($(this).is('[required]')) _class = 'error-class';
	if(!$(this).hasClass('error-class') && !$(this).hasClass('warning-class')) $(this).addClass(_class);
    throw text;
}
$.prototype.validateIsNotImportant = function(text = '')
{
	console.log(text);
	let _class = 'warning-class';
	if($(this).is('[required]')) _class = 'error-class';
	if(!$(this).hasClass('warning-class') && !$(this).hasClass('error-class')) $(this).addClass(_class);
}
